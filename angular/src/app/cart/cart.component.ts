import { Component, OnInit } from '@angular/core';
import { ShoppingService } from '../_services/shopping.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.less']
})
export class CartComponent implements OnInit {

  constructor(public shoppingService: ShoppingService) { }

  ngOnInit() {
  }

}
