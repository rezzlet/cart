import { TestBed, async } from '@angular/core/testing';
import { ShoppingService } from './shopping.service';
import { HttpClientModule } from '@angular/common/http';

const cartItem = {name: 'test', pvp: 0};

describe('ShoppingService', () => {
  let subject: ShoppingService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule ],
      providers: [
        ShoppingService
      ],
    }).compileComponents();
    subject = TestBed.get(ShoppingService);
    subject.addToCart(cartItem);
  }));

  it('should retrieve catalog from static json', () => {});
  it('should add new item to cart', () => {
    expect(subject.cartItems.length).toEqual(1);
  });
  it('should do nothing when trying to add an item that is already added to cart', () => {
    subject.addToCart(cartItem);
    expect(subject.cartItems.length).toEqual(1);
  });
  it('should detect items already on cart', () => {
    expect(subject.isAlreadyOnCart(cartItem)).toBeTruthy();
  });
  it('should return catalog items', () => {
    expect(subject.items).toBeTruthy();
  });
  it('should return the total amount to pay for current items in cart', () => {
    const cartItem2 = {name: 'test2', pvp: 2};
    subject.addToCart(cartItem2);
    expect(subject.cartTotalAmount).toEqual(2);
  });
  it('should return items in cart', () => {
    expect(subject.cartItems).toBeTruthy();
  });

});
