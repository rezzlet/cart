import { Injectable } from '@angular/core';
import { Item } from '../_entities/item';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ShoppingService {
  private catalog: Item[] = [];
  private cart: Item[] = [];

  constructor(private http: HttpClient) {
        this.http.get<Item[]>('/assets/items.json').subscribe((rs) => {
            this.catalog = rs;
        });
  }

  public addToCart(item: Item) {
    if (!this.isAlreadyOnCart(item)) {
       this.cart.push(item);
    }
  }

  public isAlreadyOnCart(item: Item) {
    return this.cart.find((curItem) => curItem === item);
  }

  public get items(): Item[] {
      return this.catalog;
  }

  public get cartTotalAmount(): number {
    return this.cart.reduce((prev, curr) => prev + curr.pvp, 0);
  }

  public get cartItems(): Item[] {
    return this.cart;
  }

}
