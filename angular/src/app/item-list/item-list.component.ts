import { Component, OnInit } from '@angular/core';
import { ShoppingService } from '../_services/shopping.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.less']
})
export class ItemListComponent implements OnInit {
  constructor(public shoppingService: ShoppingService) { }

  ngOnInit() {
  }

}
