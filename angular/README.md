# Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.0.

## Setting up environment

Run `npm install` to grab dependencies from npm.
Run `npm install -g @angular/cli` to install Angular CLI.

## Development server

Run `ng serve` for a dev server. This command will build bundles and execute a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Building for SSR

Run `npm run build:ssr` for both client and server bundles.

## Start SSR server

Run `npm run serve:ssr` for a server side rendered setup. Navigate to `http://localhost:4000`. 

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
